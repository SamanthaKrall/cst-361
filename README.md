# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

In this course, students will work in teams of two to design and build an Internet of Things (IoT) application as shown in Figure 1. The application will be designed to leverage eight common design patterns often found in an Enterprise Java application.


 




Figure 1 <insert title>
The design and code will support the following high-level functional requirements:
Embedding
The IoT application is required to generate data; therefore, students will be given the option to write an application to emulate the IoT embedded device functionality or if available, students can chose to use an actual embedded device, such as a Raspberry Pi with appropriate sensors. It recommended that students keep this simple, as the focus of this course is not on developing the IoT applications or working with embedded devices.
Back End Service
The IoT application is required to process and store the data; therefore, students will design a back-end service using Enterprise Java technologies for a REST-based API that will be consumed by the IoT embedded application. The REST API should not be anonymous and leverage at a minimum HTTP Basic Authentication for securing the REST API endpoint. The implementation of the REST API should simply be a façade over any business logic required to process the IoT data and store the data in a relational database. It is recommended that the students keep the data model design as simple as possible to meet the project requirements, as this course is not focused on database design. The JavaDB or MySQL database can easily suffice for the project.
Front End Web Application
The IoT application is required to tailor to the user; therefore, students will design a front-end web application using Enterprise Java technologies to implement a simple IoT Reporting application. The Reporting application will leverage a number of common design patterns and should provide a tabular data report as well as a visual chart based report. It will be important to research available open source charting libraries and/or JSF components that could be leveraged in the final solution.
Project Management
Each group will leverage and apply the Scrum methodology practiced in CST-247 to manage the delivery of the team project. 
For a refresh on Scrum, review the following resources:
http://www.scrumguides.org/ 
https://www.scrum.org/resources/what-is-scrum 
Project Milestones
The team project is designed and built using an iterative approach and delivered using the milestones outlined below. It should be noted that all milestones include a design report. However, the application code will be used in all milestones except for Milestones 1 and 2. 
Project Documentation
Documentation of all technical decisions and technical designs will be via a formal design report that captures all appropriate UML diagrams, ER diagrams, UI designs, and other technical artifacts to support the design of the end-to-end solution and application. Refer to the "Design Report Template" located within the Course Materials for detailed instructions.
The Design Report at a minimum will contain the following technical elements:
1.	Cover Page: Outline a summary of the project assignment objectives and team member tasks.
2.	UML Diagrams: Use case diagrams for all requirements, component diagrams for solution, and class diagrams for all non-framework classes.
3.	User Interface Designs: Use wireframe designs for all screens.
4.	Database Design: Use an ER diagram.
5.	Test Plan: Test cases for all functionality.


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Samantha Krall
Grand Canyon University
CST-361